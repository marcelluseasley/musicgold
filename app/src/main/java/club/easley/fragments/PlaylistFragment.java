package club.easley.fragments;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import club.easley.adapters.SongAdapter;
import club.easley.musicgold.MainActivity;
import club.easley.musicgold.R;
import club.easley.musicgold.Song;

/**
 * Created by measley on 8/19/2015.
 */
public class PlaylistFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView songView;
    ArrayList<Song> fragSongs;
    AudioManager am;
    MediaPlayer mp;


    InputMethodManager inputManager;
    final String TAG = "PLAYLISTS";


    private String playlistNewName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.playlistfragment_layout, container, false);

        playlistNewName="";

        inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((null == getActivity().getCurrentFocus()) ? null : getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);


        songView = (ListView)v.findViewById(R.id.song_list);
        fragSongs = ((MainActivity)getActivity()).getSongArray();
        SongAdapter songAdt = new SongAdapter(v.getContext(), fragSongs );
        songView.setFastScrollEnabled(true);
        songView.setAdapter(songAdt);


        checkforplaylists(getActivity().getApplicationContext());

        setHasOptionsMenu(true);
        return v;
    }

    public void checkforplaylists(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        final Uri uri=MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        final String id=MediaStore.Audio.Playlists._ID;
        final String name=MediaStore.Audio.Playlists.NAME;
        final String[]columns={id,name};
        final Cursor playlists= cr.query(uri, columns, null, null, null);
        if(playlists==null)
        {
            Log.e(TAG, "Found no playlists.");
            return;
        }else {
            Log.d(TAG, String.format("Playlist count is %d", playlists.getCount()));
            playlists.moveToFirst();
            for(int i = 0; i< playlists.getCount(); i++){
                playlists.moveToPosition(i);
                int playlistID = playlists.getInt(playlists.getColumnIndex(MediaStore.Audio.Playlists._ID));
                String playlistName = playlists.getString(playlists.getColumnIndex(MediaStore.Audio.Playlists.NAME));
                Log.d(TAG, playlistName+ " " + String.format("%d", playlistID));

            }

        }
        return;
    }

   

    public void createPlaylist(String pName){

        final Uri uri=MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        final String id=MediaStore.Audio.Playlists._ID;
        final String name=MediaStore.Audio.Playlists.NAME;
        final String[]columns={id,name};

        ContentResolver contentResolver = getActivity().getApplicationContext().getContentResolver();
        Uri playlists = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        Cursor c = contentResolver.query(playlists, columns, null, null,
                null);
        long playlistId = 0;
        c.moveToFirst();
        do {
            String plname = c.getString(c
                    .getColumnIndex(MediaStore.Audio.Playlists.NAME));
            if (plname.equalsIgnoreCase(pName)) {
                playlistId = c.getLong(c
                        .getColumnIndex(MediaStore.Audio.Playlists._ID));
                break;
            }
        } while (c.moveToNext());
        c.close();

        if (playlistId != 0) {
            //playlist already exists
            Toast.makeText(getActivity().getBaseContext(),"Playlist " +pName+ " already exists.",Toast.LENGTH_SHORT).show();

            /*Uri deleteUri = ContentUris.withAppendedId(playlists, playlistId);
            Log.d(TAG, "REMOVING Existing Playlist: " + playlistId);

            // delete the playlist
            contentResolver.delete(deleteUri, null, null);*/
        }else{
            ContentValues v1 = new ContentValues();
            v1.put(MediaStore.Audio.Playlists.NAME, pName);
            v1.put(MediaStore.Audio.Playlists.DATE_MODIFIED,
                    System.currentTimeMillis());
            Uri newpl = contentResolver.insert(playlists, v1);
            Toast.makeText(getActivity().getBaseContext(),"Playlist " +pName+ " created.",Toast.LENGTH_SHORT).show();
        }
    }




    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_playlist_add){
            Log.d("MENU ITEM CLICK", "PLAYLIST_ADD");
            //Toast.makeText(getActivity().getBaseContext(),"Add playlist",Toast.LENGTH_LONG).show();

            LayoutInflater layoutInflater = LayoutInflater.from(getActivity().getBaseContext());

            View promptView = layoutInflater.inflate(R.layout.prompts, null);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

            // set prompts.xml to be the layout file of the alertdialog builder
            alertDialogBuilder.setView(promptView);

            final EditText input = (EditText) promptView.findViewById(R.id.userInput);

            // setup a dialog window
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // get user input and set it to result
                            //Toast.makeText(getActivity().getBaseContext(),input.getText().toString().trim(),Toast.LENGTH_LONG).show();
                            createPlaylist(input.getText().toString().trim());
                        }
                    })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,	int id) {
                                    dialog.cancel();
                                }
                            });

            // create an alert dialog
            AlertDialog alertD = alertDialogBuilder.create();

            alertD.show();



            //((MainActivity)getActivity()).refreshSongs();
            //updateAdapter();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        // Do something that differs the Activity's menu here
        menu.findItem(R.id.action_playlist_add).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        if (mp != null)
            mp.release();
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }



}
